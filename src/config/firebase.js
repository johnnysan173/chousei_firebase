import firebase from 'firebase';

// const firebaseConfig = {
//   apiKey: "xxx",
//   authDomain: "xxx",
//   databaseURL: "xxx",
//   projectId: "xxx",
//   storageBucket: "xxx",
//   messagingSenderId: "xxx",
//   appId: "xxx"
// };

const firebaseConfig = {
  apiKey: "AIzaSyDpHs7uxEC6K4V8M1fTnMX3crl54zD8Y_E",
  authDomain: "chousei-firebase-6feb4.firebaseapp.com",
  databaseURL: "https://chousei-firebase-6feb4.firebaseio.com",
  projectId: "chousei-firebase-6feb4",
  storageBucket: "chousei-firebase-6feb4.appspot.com",
  messagingSenderId: "244616396213",
  appId: "1:244616396213:web:399d34c1b1e8b6a6922dfc"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
